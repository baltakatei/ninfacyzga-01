#!/bin/bash

# Date: 2020-04-10T04:32Z

# Author: Steven Baltakatei Sandoval

# License: 

# Description: Logs pressure to a daily log file. Restarts daily at
# UTC midnight.

# Usage: plog.sh [ sensor script path ] [ output log dir path ]

echoerr() { echo "$@" 1>&2 ; } # Define echo to stderr function.

SENSOR_SCRIPT_PATH="$1" # Identify first argument as sensor script path.
DIROUT1="$2" # Identify second argument as output log file directory path.

SCRIPT_PATH="${0}" # [1]
SCRIPT_BASENAME="$(basename "$SCRIPT_PATH")"
SCRIPT_DIR="$(dirname "$SCRIPT_PATH")"
SCRIPT_TIME_SHORT="$(date +%Y%m%dT%H%M%S%z)"
SCRIPT_DATE_SHORT="$(date +%Y%m%d)"

SCRIPT_ERROR_LOG_PATH=~/"$SCRIPT_DATE_SHORT""_""$SCRIPT_BASENAME"..error.log

# Check that time synchronization service ('ntp') exists. Warn if it doesn't.
if command -v ntp 1>/dev/null 2>&1; then echoerr "WARNING: ntp not found. Log timestamps may not be accurate."; fi

# Main loop
if [ -d "$DIROUT1" ] && [ -f "$SENSOR_SCRIPT_PATH" ] ; then # Check that output directory and sensor script file exist.
    while true; do
	echoerr "DEBUG:Starting while loop."
	
	# Update time constants
	TIME_CURRENT="$(date --iso-8601=seconds)" ;
	#echoerr "DEBUG:TIME_CURRENT is:""$TIME_CURRENT" ;
	TIME_CURRENT_SHORT="$(date -d "$TIME_CURRENT" +%Y%m%dT%H%M%S%z)"
	#echoerr "DEBUG:TIME_CURRENT_SHORT is""$TIME_CURRENT_SHORT"
	DATE_CURRENT="$(date -d "$TIME_CURRENT" --iso-8601=date)" ;
	#echoerr "DEBUG:DATE_CURRENT is:""$DATE_CURRENT" ;
	DATE_CURRENT_SHORT="$(date -d "$TIME_CURRENT" +%Y%m%d)" ;
	#echoerr "DEBUG:DATE_CURRENT is:""$DATE_CURRENT_SHORT" ;
	DATE_TOMORROW="$(date -d "$TIME_CURRENT next day" --iso-8601=date)" ;
	#echoerr "DEBUG:DATE_TOMORROW is:""$DATE_TOMORROW" ;
	TIME_NEXT_MIDNIGHT="$(date -d "$DATE_TOMORROW" --iso-8601=seconds)" ;
	#echoerr "DEBUG:TIME_NEXT_MIDNIGHT is:""$TIME_NEXT_MIDNIGHT" ;
	####SECONDS_UNTIL_NEXT_MIDNIGHT="$((  $(date +%s -d "$TIME_NEXT_MIDNIGHT") - $(date +%s -d "$TIME_CURRENT")  ))" ;
	SECONDS_UNTIL_NEXT_MIDNIGHT="10" # DEBUG 10 second. Hail Satan.
	#echoerr "DEBUG:SECONDS_UNTIL_NEXT_MIDNIGHT is:""$SECONDS_UNTIL_NEXT_MIDNIGHT" ;
	if [ "$SECONDS_UNTIL_NEXT_MIDNIGHT" -eq 0 ]; then echoerr "WARNING:Is it exactly midnight?" ; continue; fi ; # Exit loop early if equal to 0 because "timeout 0s" never ends.
	if [ "$SECONDS_UNTIL_NEXT_MIDNIGHT" -lt 0 ]; then echoerr "ERROR:Time until midnight error." ; exit 1; fi ; # Exit script if equal to 0 because "timeout 0s" never ends.
	
	# Update output file names.
	DIROUTN="$DIROUT1" ;
	FILEOUTN="$DATE_CURRENT_SHORT""..""$(hostname)""_pressure.csv" ;

	# DEBUG
	echoerr "SECONDS_UNTIL_NEXT_MIDNIGHT:""$SECONDS_UNTIL_NEXT_MIDNIGHT"
	echoerr "SENSOR_SCRIPT_PATH         :""$SENSOR_SCRIPT_PATH"
	echoerr "DIROUTN                    :""$DIROUTN"
	echoerr "FILEOUTN                   :""$FILEOUTN"
	
	# Append pressure log until midnight.
	timeout "$SECONDS_UNTIL_NEXT_MIDNIGHT""s" python2 "$SENSOR_SCRIPT_PATH" 1>> "$DIROUTN"/"$FILEOUTN" 2>&1 ; # log script data
    done
    
else
    echoerr "$SCRIPT_TIME_SHORT"">Sensor script file not found at: $SENSOR_SCRIPT_PATH or $DIROUT1 not found."
    echo "$SCRIPT_TIME_SHORT"">Sensor script file not found at: $SENSOR_SCRIPT_PATH or $DIROUT1 not found." >> "$SCRIPT_ERROR_LOG_PATH" ; 
fi

# == References ==
# 1. How to indicate script file name in a bash script. https://stackoverflow.com/a/3588939
