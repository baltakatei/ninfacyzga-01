<!DOCTYPE html>
<meta charset="utf-8">

# Environment Sensor ninfacyzga-01

Created by [Steven Baltakatei Sandoval][bktei_2020_homepage] on
2020-06-07T21:56Z under a [CC BY-SA 4.0][cc_20131125_bysa] license and
last updated on 2020-06-28T16:41Z.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Environment Sensor ninfacyzga-01](#environment-sensor-ninfacyzga-01)
    - [Abstract](#abstract)
    - [Setup](#setup)
        - [Location](#location)
    - [Name Etymology](#name-etymology)
        - [Origin](#origin)
        - [Meaning](#meaning)
        - [Pronunciation](#pronunciation)
    - [Directory structure](#directory-structure)
        - [Subsection](#subsection)
    - [References](#references)

<!-- markdown-toc end -->


## Abstract

This git repository contains software and documentation necessary to
set up a sensor package for logging new fact observe.

Sensor data collected include:

- location (GPS WGS84)

## Setup

See the following documents for instructions on setting up hardware
for each type of data collected.

### Location

See files within [`doc/location`](doc/location).

## Name Etymology

### Origin

The name "ninfacyzga" is a
lojban<sup>[[1]](#lojwiki_20200212_main)</sup>
lujvo<sup>[[2]](#lojwiki_20140930_lujvo)</sup> consisting of the tanru
"cnino fatci zgana". The three gismu in the tanru contain the
following meanings:

* **cnino**: new
* **fatci**: fact
* **zgana**: observe

### Meaning

Hence, the primary mission of a ninfacyzga device is to observe facts
of the new. The primary mission is NOT to process the facts into
stories since that is the mission of a temlisru'e device. Neither is
the primary mission to network with other devices over distances of
space for the purpose of sharing information.

In practice, this means a ninfacyzga device records data about the
environment but does not attempt to generate summaries or averages of
the data. It simply transmits what it sees (ex: camera), hears (ex:
microphone), feels (ex: accelerometer), smells (ex: gas detector),
mags (ex: magnetometer).

### Pronunciation

"ninfacyzga" is pronounced /ninfaʃəzga/.

## Directory structure

Directories in this repository are structured as follows:

* `doc`: Contains documentation explaining in human-readable format
  what each file does.

* `archive`: Contains legacy files from previous iterations of the
  project.
  
* `exec`: Contains executable components of the project.

### Subsection

## References
- <a name="lojwiki_20200212_main">1.</a> ["Lojban"][1]. 2020-02-12. [Logical Language Group"](https://mw.lojban.org/papri/Logical_Language_Group). Date Accessed: 2020-06-07. [Archive link](https://web.archive.org/web/20200304102316/https://mw.lojban.org/papri/Logical_Language_Group). Archive date: 2020-03-04.
- <a name="lojwiki_20140930_lujvo">2.</a> ["lujvo"][2]. 2014-09-30. [Logical Language Group"](https://mw.lojban.org/papri/Logical_Language_Group). Date Accessed: 2020-06-07. [Archive link](https://web.archive.org/web/20160401100124/https://mw.lojban.org/papri/lujvo). Archive date: 2016-04-01.

[1]: https://mw.lojban.org/papri/Lojban
[2]: https://mw.lojban.org/papri/lujvo
[bktei_2020_homepage]: http://baltakatei.com
[cc_20131125_bysa]: http://creativecommons.org/licenses/by-sa/4.0/


<hr>
<p xmlns:dct="http://purl.org/dc/terms/" xmlns:cc="http://creativecommons.org/ns#">This work by <a rel="cc:attributionURL"  href="http://baltakatei.com"><span rel="cc:attributionName">Steven Baltakatei Sandoval</span></a> is licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/?ref=ccchooser" target="_blank" rel="license noopener noreferrer" style="display: inline-block;">CC BY-SA 4.0</a><a href="https://creativecommons.org/licenses/by-sa/4.0/?ref=ccchooser"><img style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc-by_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc-sa_icon.svg" /></a></p>
